# Name
Generalised Framework For Extraction of Meaningful Information

## Problem Statement
Heterogeneous sources of content is available online. This information available is raw, unorganized and ambiguous to understand. The solution to this problem to create a generalized framework for extracting meaningful data from websites using web crawlers perform data mining to present it via visual analytic.

## Description
The WWW(World Wide Web) is the richest source of information and it keeps growing day by day. 
Search Engines are designed to access massive content using web crawlers. 
Recent challenges in web mining deals with extracting raw contents, organizing and discovering knowledge from it. 
This is relevant especially in the domain of medicine, diseases, employment statistics of the country and many more. Extracting this data would play a major role for assisting researcher in their respective domains to gain insights of the data. 


## Interested ? Need more Details ?
Visit [PPT](https://gitlab.com/AbhishekSatbhai/generalised-framework-for-extraction-of-meaningful-information/-/blob/main/BE%20Project%20PPT%2005-03-2019.pptx) for this information.


## Authors and acknowledgment
Abhishek Satbhai
Chinmayi Shaligram
Rohit Tulsiani


## Project status
Project Completed, development is stopped completely. If someone wants to choose to fork or volunteer to keep going you are welcome :)

