﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;

namespace WEBSITE1
{
    public partial class diseaseIndia : System.Web.UI.Page
    {
        string link1 ="";
           // "http://blockchain.sakmarketing.in/Home.aspx";
        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Mrunal\WEBSITE1\WEBSITE1\App_Data\state.accdb");
        protected void Page_Load(object sender, EventArgs e)
        { con.Open();
            //OleDbCommand cm = new OleDbCommand("delete from India", con);
            //cm.ExecuteNonQuery();
            link1 = Session["LinkUrl"].ToString();
            System.Net.WebClient wc = new System.Net.WebClient();
           byte[] raw = wc.DownloadData(link1);

            string webData = System.Text.Encoding.UTF8.GetString(raw);
            string[] data = webData.Split('\n');
            int flg = 0;
            for (int a = 0; a < data.Length; a++)
            {
                string srno = "";
                string state = "";
                string year = "";
                string dname = "";
                string dcount = "";
                if (data[a].Contains("GridView1"))
                {
                    flg = 1;
                }
                if (flg == 1 && data[a].Contains("</table>"))
                {
                    flg = 0;
                }
                if (flg == 1)
                {
                    string lined = data[a].ToString();
                    string[] ldata = lined.Split('>');
                    for (int k = 0; k < ldata.Length; k++)
                    {
                        if (a >= 22)
                        {

                            string[] srno1 = ldata[k].Split('<');
                            if (k == 2)
                            {
                                srno = srno1[0];
                            }
                            if (k == 6)
                            {
                                state = srno1[0];
                            }
                            if (k == 10)
                            {
                                year = srno1[0];
                            }
                            if (k == 14)
                            {
                                dname = srno1[0];
                            }
                            if (k == 18)
                            {
                                dcount = srno1[0];
                            }
                        }
                    }
                    if (srno != "" && srno != "\r" && dname != "" && state != "&nbsp;")
                    {

                       OleDbCommand cmd = new OleDbCommand("Insert into India values(" + srno + ",'" + state.Trim() + "','" + year.Trim() + "','" + dname.Trim() + "'," + dcount + ")", con);
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            con.Close();

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
             DropDownList2.Items.Clear();
            if (DropDownList1.Text == "Year Wise")
            {
                Label2.Text = "Select Year";

                DataSet ds = new DataSet();
                con.Open();
                OleDbDataAdapter adapt = new OleDbDataAdapter("Select distinct(Year) from India", con);
                adapt.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DropDownList2.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                }
                con.Close();
            }
            else if (DropDownList1.Text == "State Wise")
            {
                Label2.Text = "Select State";

                DataSet ds = new DataSet();
                con.Open();
               OleDbDataAdapter adapt = new OleDbDataAdapter("Select distinct(state) from India", con);
                adapt.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DropDownList2.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                }
                con.Close();
            }
            else
            {
                Label2.Text = "Select Disease";

                DataSet ds = new DataSet();
                con.Open();
                OleDbDataAdapter adapt = new OleDbDataAdapter("Select distinct(dname) from India", con);
                adapt.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DropDownList2.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                }
                con.Close();
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Chart1.Series.Clear();
            Chart1.Titles.Clear();
            if (DropDownList1.Text == "Year Wise")
            {
                string val = DropDownList2.Text;
                // con.Open();
                OleDbDataAdapter adapt = new OleDbDataAdapter("Select Distinct(state) from India where Year='" + val + "'", con);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                List<string> ls = new List<string>();
                Chart1.Titles.Add("Disease Chart");
                //     Chart1.Series.Add("State");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string dname = ds.Tables[0].Rows[i][0].ToString().Trim();

                    if (ls.Contains(dname))
                    {
                    }
                    else
                    {
                        // Arunachal Pradesh
                        con.Open();
                        OleDbDataAdapter adapt1 = new OleDbDataAdapter("Select * from India where year='" + val + "' and state='" + dname + "'", con);
                        DataSet ds1 = new DataSet();
                        adapt1.Fill(ds1);
                        int dcount = 0;
                        for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                        {
                            dcount += int.Parse(ds1.Tables[0].Rows[j][4].ToString());
                        }
                        if (dcount > 0)
                        {
                            Chart1.Series.Add(dname);
                            Chart1.Series[dname].ToolTip = dname + " " + dcount;
                            Chart1.Series[dname].Points.AddXY(ds.Tables[0].Rows[i][0].ToString(), dcount);
                            Chart1.Series[dname].XValueMember = val;
                            Chart1.Series[dname].YValueMembers = val;
                        }
                        ls.Add(dname.ToString());
                        con.Close();
                    }
                }

                con.Close();
            }
            else if (DropDownList1.Text == "State Wise")
            {
                string val = DropDownList2.Text;
                // con.Open();
                OleDbDataAdapter adapt = new OleDbDataAdapter("Select Distinct(dname) from India where state='" + val + "'", con);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                List<string> ls = new List<string>();
                Chart1.Titles.Add("Disease Chart");
                //     Chart1.Series.Add("State");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string dname = ds.Tables[0].Rows[i][0].ToString().Trim();

                    if (ls.Contains(dname))
                    {
                    }
                    else
                    {
                        // Arunachal Pradesh
                        con.Open();
                        OleDbDataAdapter adapt1 = new OleDbDataAdapter("Select * from India where state='" + val + "' and dname='" + dname + "'", con);
                        DataSet ds1 = new DataSet();
                        adapt1.Fill(ds1);
                        int dcount = 0;
                        for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                        {
                            dcount += int.Parse(ds1.Tables[0].Rows[j][4].ToString());
                        }
                        if (dcount > 0)
                        {
                            Chart1.Series.Add(dname);
                            Chart1.Series[dname].ToolTip = dname + " " + dcount;
                            Chart1.Series[dname].Points.AddXY(ds.Tables[0].Rows[i][0].ToString(), dcount);
                            Chart1.Series[dname].XValueMember = val;
                            Chart1.Series[dname].YValueMembers = val;
                        }
                        ls.Add(dname.ToString());
                        con.Close();
                    }
                }

                con.Close();
            }
            else
            {
                string val = DropDownList2.Text;
                // con.Open();
                OleDbDataAdapter adapt = new OleDbDataAdapter("Select Distinct(state) from India where dname='" + val + "'", con);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                List<string> ls = new List<string>();
                Chart1.Titles.Add("Disease Chart");
                //     Chart1.Series.Add("State");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string dname = ds.Tables[0].Rows[i][0].ToString().Trim();

                    if (ls.Contains(dname))
                    {
                    }
                    else
                    {
                        // Arunachal Pradesh
                        con.Open();
                        OleDbDataAdapter adapt1 = new OleDbDataAdapter("Select * from India where dname='" + val + "' and state='" + dname + "'", con);
                        DataSet ds1 = new DataSet();
                        adapt1.Fill(ds1);
                        int dcount = 0;
                        for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                        {
                            dcount += int.Parse(ds1.Tables[0].Rows[j][4].ToString());
                        }
                        if (dcount > 0)
                        {
                            Chart1.Series.Add(dname);
                            Chart1.Series[dname].ToolTip = dname + " " + dcount;
                            Chart1.Series[dname].Points.AddXY(ds.Tables[0].Rows[i][0].ToString(), dcount);
                            Chart1.Series[dname].XValueMember = val;
                            Chart1.Series[dname].YValueMembers = val;
                        }
                        ls.Add(dname.ToString());
                        con.Close();
                    }
                }

                con.Close();
            }

        }
    }







}